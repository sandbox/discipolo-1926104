core = 7.x
api = 2


; Modules

projects[features][version] = "2.x-dev"
projects[features][patch][] = "http://drupal.org/files/features-986968-24.patch"
projects[] = fivestar
projects[] = flag
projects[getid3][version] = 1.x-dev
projects[] = mediafront
projects[] = views
projects[] = views_rss
projects[] = views_rss_itunes
projects[] = votingapi
projects[navbar][version] = "1.x-dev"
projects[dynamic_background][version] = "2.x-dev"
projects[uuid][version] = "1.x-dev"

; Devtools
projects[] = "node_convert"
projects[] = "diff"

; this should be in biblio?

projects[] = "better_exposed_filters"
projects[] = "views_bulk_operations"

; Custom features

projects[pushlib_management][type] = "module"
projects[pushlib_management][download][type] = "git"
projects[pushlib_management][download][url] = "http://git.drupal.org/sandbox/discipolo/1926378.git"
projects[pushlib_management][download][branch] = "7.x-1.x"

projects[pushlib_pages][type] = "module"
projects[pushlib_pages][download][type] = "git"
projects[pushlib_pages][download][url] = "http://git.drupal.org/sandbox/discipolo/1926384.git"
projects[pushlib_pages][download][branch] = "7.x-1.x"

projects[pushlib_news][type] = "module"
projects[pushlib_news][download][type] = "git"
projects[pushlib_news][download][url] = "http://git.drupal.org/sandbox/discipolo/1926388.git"
projects[pushlib_news][download][branch] = "7.x-1.x"

projects[pushlib_publications][type] = "module"
projects[pushlib_publications][download][type] = "git"
projects[pushlib_publications][download][url] = "http://git.drupal.org/sandbox/discipolo/1926364.git"
projects[pushlib_publications][download][branch] = "7.x-1.x"

projects[pushlib_events][type] = "module"
projects[pushlib_events][download][type] = "git"
projects[pushlib_events][download][url] = "http://git.drupal.org/sandbox/discipolo/2039543.git"
projects[pushlib_events][download][branch] = "7.x-1.x"

projects[mogul][type] = "module"
projects[mogul][download][type] = "git"
projects[mogul][download][url] = "http://git.drupal.org/project/mogul.git"
projects[mogul][download][branch] = "7.x-1.x"
projects[mogul][patch][2041303] = "https://drupal.org/files/mogul_podcast-update-2041303-5.patch"
; Libraries

;Themes
projects[flux][version] = "1.x-dev"
projects[omega][version] = "4.x-dev"
projects[tamal][version] = "1.0"
projects[stanley][version] = "2.x-dev"
projects[adaptivetheme][version] = "3.x-dev"
projects[corolla][version] = "3.x-dev"

projects[pushtheme][type] = "theme"
projects[pushtheme][download][type] = "git"
projects[pushtheme][download][url] = "http://git.drupal.org/sandbox/discipolo/1719258.git"
projects[pushtheme][download][branch] = "7.x-1.x"

